# YourCX recruitment task

Dla lepszego zrozumienia i uniknięcia niedomówień rozwiązanie przygotowałem w formie repozytorium z pustymi klasami. 

Ponieważ nie są znane wszystkie szczegóły systemu dokonałem pewnych założeń, do których dostosowałem strukturę klas. Struktura została zaprojektowana w użyciem IoC, zgodnie z zasadami SOLID, DRY, KISS oraz następujących wzorców projektowych:

- Fabryka
- Iterator
- Metoda szablonowa
- Repozytorium
- Strategia

## Przyjęte założenia

Parametr `$importId` w kontrolerze `ImportController` wskazuje na pojedynczy rekord w tabeli z informacjami o plikach i podczas wykonywania akcji przetwarzamy tylko jeden plik.

Tabela, w której trzymamy rekordy z informacjami o plikach nazywa się `imports` i zawiera następujące kolumny:

- id
- file_type
- path

Tabela, do której importujemy dane nazywa się `surveys` jako, że "wypełnienia badań zrealizowanych na innej platformie" kojarzą mi się z wynikami jakichś ankiet. Tabela posiada tylko dwie kolumny, ponieważ nie została podana struktura tych danych:

- id
- data   

Plik w formacie JSON posiada strukturę podobną do pozostałych plików - wszelkie dane dla jednego rekordu zawierają się w jednej linii.

Nie mamy doczynienia z Big Data i zawartość plików, które odczytujemy możemy bez obaw załadować w całości do pamięci.

## Struktura klas

![Class diagram](class_diagram.png)

## Rozwiązania problemów zawartych w zadaniu

### Obsługa różnych formatów plików

W celu obsługi wielu formatów plików użyty został wzorzec Strategii. Pozwala on na łatwą podmianę klasy, która potrafi obsłużyć dany format pliku. Za odpowiedni wybór i tworzenia klasy odpowiedzialna jest Fabryka.

### Proste przetworzenie wierszy

Dla łatwego przetwarzania wierszy `FileReader` implementuje interfejs Iterator dzięki któremu możemy w łatwy sposób w pętli `foreach` przetwarzać pojedyncze wiersze. `FileReader` implementuje również interfejs `Countable`, który umożliwia uzyskanie liczby wierszy za pomocą standardowej funkcji `count`.

### Operacje na pojedynczym pliku

Wszelkie operacje na pojedynczym pliku opisane w zadaniu można dokonać za pomocą poszczególnych metod (odpowiednio `getColumns`, `getRowCount`, `getRows`) z serwisu `ImportService`. 

### Mechanizm, który przetworzy plik i zaimportuje jego zawartość do bazy

Trudno mi ocenić, w którym miejscu ten mechanizm jest potrzebny. Zakładam więc, że służyć do tego będzie osobna akcja w kontrolerze `ImportController`. W tym celu należy w pierszej kolejności użyć metody `getRows` z serwisu `ImportService`. Następnie uzyskane dane zapisać do bazy za pomocą metody `save` w `SurveyService`. Przykład zawarłem w kontrolerze `ImportController` w metodzie `importRows`.

## Informacje dodatkowe

Przy implementacji zapewne pojawiłyby się metody pomocnicze w klasach, w szczególności w klasach `FileReader`.  