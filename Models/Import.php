<?php

namespace Models;

use ValueObjects\FileType;

class Import
{
    private ?int $id;
    private FileType $fileType;
    private string $path;

    public function __construct(?int $id, FileType $fileType, string $path)
    {
        $this->id = $id;
        $this->fileType = $fileType;
        $this->path = $path;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFileType(): FileType
    {
        return $this->fileType;
    }

    public function setFileType(FileType $fileType): void
    {
        $this->fileType = $fileType;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function setPath(string $path): void
    {
        $this->path = $path;
    }
}
