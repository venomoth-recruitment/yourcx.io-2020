<?php

namespace Models;

class Survey
{
    private ?int $id;
    private array $data;

    public function __construct(?int $id, array $data)
    {
        $this->id = $id;
        $this->data = $data;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getData(): array
    {
        return $this->data;
    }

    public function setData(array $data): void
    {
        $this->data = $data;
    }
}
