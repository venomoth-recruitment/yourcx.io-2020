<?php

namespace Services;

use Readers\FileReaderFactoryInterface;
use Repositories\ImportRepositoryInterface;

class ImportService implements ImportServiceInterface
{
    private ImportRepositoryInterface $importRepository;
    private FileReaderFactoryInterface $fileReaderFactory;

    public function __construct(
        ImportRepositoryInterface $importRepository,
        FileReaderFactoryInterface $fileReaderFactory
    ) {
        $this->importRepository = $importRepository;
        $this->fileReaderFactory = $fileReaderFactory;
    }

    public function getColumns(int $importId): array
    {
    }

    public function getRowCount(int $importId): int
    {
    }

    public function getRows(int $importId): array
    {
    }
}
