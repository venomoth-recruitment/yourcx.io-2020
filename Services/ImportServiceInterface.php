<?php

namespace Services;

interface ImportServiceInterface
{
    public function getColumns(int $importId): array;

    public function getRowCount(int $importId): int;

    public function getRows(int $importId): array;
}
