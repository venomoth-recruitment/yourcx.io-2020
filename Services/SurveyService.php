<?php

namespace Services;

use Repositories\SurveyRepositoryInterface;

class SurveyService implements SurveyServiceInterface
{
    private SurveyRepositoryInterface $surveyRepository;

    public function __construct(SurveyRepositoryInterface $surveyRepository) {
        $this->surveyRepository = $surveyRepository;
    }

    public function save(array $data): void
    {
    }
}
