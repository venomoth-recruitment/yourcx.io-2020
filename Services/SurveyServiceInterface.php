<?php

namespace Services;

interface SurveyServiceInterface
{
    public function save(array $data): void;
}
