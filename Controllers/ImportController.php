<?php

namespace Controllers;

use Services\ImportServiceInterface;
use Services\SurveyServiceInterface;

class ImportController
{
    private ImportServiceInterface $importService;
    private SurveyServiceInterface $surveyService;

    public function __construct(ImportServiceInterface $importService, SurveyServiceInterface $surveyService)
    {
        $this->importService = $importService;
        $this->surveyService = $surveyService;
    }

    public function showColumns(int $importId): array
    {
        return $this->importService->getColumns($importId);
    }

    public function importRows(int $importId)
    {
        $data = $this->importService->getRows($importId);
        $this->surveyService->save($data);
    }
}