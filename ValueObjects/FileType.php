<?php

namespace ValueObjects;

class FileType
{
    public const CSV = 'csv';
    public const JSON = 'json';
    public const XLSX = 'xlsx';

    private const VALID_FILE_TYPES = [
        self::CSV,
        self::JSON,
        self::XLSX
    ];

    private string $value;

    public function __construct(string $value)
    {
        $this->value = $value;
    }

    private function isValid(string $value): bool
    {
    }

    public function getValue(): string
    {
    }
}
