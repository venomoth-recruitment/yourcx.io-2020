<?php

namespace Readers;

use ValueObjects\FileType;

interface FileReaderFactoryInterface
{
    public function create(FileType $fileType): FileReaderInterface;
}
