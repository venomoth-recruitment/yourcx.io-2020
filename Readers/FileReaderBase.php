<?php

namespace Readers;

abstract class FileReaderBase implements FileReaderInterface
{
    private string $path;

    public function __construct(string $path)
    {
        $this->path = $path;
    }

    abstract protected function parseLine(string $line): array;

    public function current()
    {
    }

    public function next()
    {
    }

    public function key()
    {
    }

    public function valid()
    {
    }

    public function rewind()
    {
    }

    public function count()
    {
    }
}
