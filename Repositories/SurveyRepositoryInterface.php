<?php

namespace Repositories;

use Models\Survey;

interface SurveyRepositoryInterface
{
    public function persist(Survey $survey): void;
}
