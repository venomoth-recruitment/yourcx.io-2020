<?php

namespace Repositories;

use Models\Import;

interface ImportRepositoryInterface
{
    public function findById(int $id): Import;
}
